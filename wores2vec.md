```
c2p.0.pbs
f2p.0.pbs
c2f: runK.sh
gunzip -c m2n5.10.gz | perl -e 'open A, "delta.id2content"; while(<A>){chop();($i,@m)=split(/\;/);$ms{$i}=join ";", @m;} while(<STDIN>){ chop();@x=split(/\;/);print "$_\;$ms{$x[0]}\n"if length($ms{$x[0]})>40;}' | gzip > n2msg5.10.40.gz
gunzip -c m2n.gz | perl -e 'open A, "delta.id2content"; while(<A>){chop();($i,@m)=split(/\;/);$ms{$i}=join ";", @m;} while(<STDIN>){ chop();@x=split(/\;/);print "$_\;$ms{$x[0]}\n" if length($ms{$x[0]})>40;}' | gzip > n2msgX.X.40.gz
```

```
seq 0 2 45| while read i
do j=$(echo $i+1|bc)
cat > cat runKR$i.pbs
#PBS -N runKR.0
#PBS -A UT-TENN0241
#PBS -l nodes=1:ppn=16,walltime=6:00:00
#PBS -j oe
#PBS -S /bin/bash
cd /lustre/medusa/audris/delta
./runKR.sh $i $j
EOF
qsub runKR$i.pbs
```

Try various cutofs

```
cat runKM0.pbs 
#PBS -N runKM.0
#PBS -A UT-TENN0241
#PBS -l nodes=1:ppn=16,walltime=6:00:00
#PBS -j oe
#PBS -S /bin/bash

gunzip -c c2pNew.ids | perl -e 'open A, "c2nf1k";while(<A>){chop();$b{$_}++;}while(<STDIN>){chop();($i,$v)=split(/ /);if(defined $b{$i}){print "$i $v\n";}}' | gzip > c2pNew.id1k &
gunzip -c c2pNew.id1k | perl -e 'open A, "c2nf10k";while(<A>){chop();$b{$_}++;}while(<STDIN>){chop();($i,$v)=split(/ /);if(defined $b{$i}){print "$i $v\n";}}' | gzip > c2pNew.id10k 
gunzip -c c2pNew.id1k | perl -e 'open A, "c2nf100k";while(<A>){chop();$b{$_}++;}while(<STDIN>){chop();($i,$v)=split(/ /);if(defined $b{$i}){print "$i $v\n";}}' | gzip > c2pNew.id100k 
gunzip -c c2pNew.id1k | perl -e 'open A, "c2nf1m";while(<A>){chop();$b{$_}++;}while(<STDIN>){chop();($i,$v)=split(/ /);if(defined $b{$i}){print "$i $v\n";}}' | gzip > c2pNew.id1m 
gunzip -c c2pNew.versions | perl -e 'open A, "gunzip -c c2pNew.id10k|";while(<A>){chop();s/^[0-9]+ //;$b{$_}++;}while(<STDIN>){chop();($i,$j)=split(/ /);if(!defined $b{$i} && $i != $j){print "$i $j\n";}}' | gzip > c2pNew.versions10k
```


```
cd /lustre/medusa/audris/delta
./runKR.sh 0 9 m2n delta.idx

seq 0 21 | while read i; do gunzip -c m2n.$i.gz; done | perl -e 'while(<STDIN>){chop();($i,$n,$e,$c)=split(/\;/, $_, -1);$m{"$i\;$n\;$e"}+=$c;}while (($k,$v)=each %m){print "$k\;$v\n";}' | gzip > m2n.gz

gunzip -c m2n.gz |perl -e 'while(<STDIN>){chop();($i,$n,$e,$c)=split(/\;/, $_, -1);$m2n{$i}{"$n\;$e"}+=$c; $n2m{"$n\;$e"}{$i}+=$c;}while (($k,$v)=each %m2n){$na=scalar(keys %{$v});print "$k\;$na\n";}' | gzip > m2na.gz
gunzip -c m2na.gz | awk -F\; '{ if ($2>5) print $1}' | sort -u > m5 &

gunzip -c m2n.gz | perl -e 'open A,"m5"; while(<A>){chop;$b{$_}++;}; while(<STDIN>){chop();($i,$n,$e,$c)=split(/\;/, $_, -1);next if defined $b{$i}; $n2m{"$n\;$e"}{$i}+=$c;}while (($k,$v)=each %n2m){@ms=(keys %{$v});if ($#ms>=9){ for $m (@ms){ print "$m\;$k\n";}}}' | gzip > m2n5.10.gz

gunzip -c m2n.gz | perl -e 'open A,"m5"; while(<A>){chop;$b{$_}++;}; while(<STDIN>){chop();($i,$n,$e,$c)=split(/\;/, $_, -1);next if defined $b{$i}; $n2m{"$n\;$e"}{$i}+=$c;}while (($k,$v)=each %n2m){@ms=(keys %{$v});if ($#ms>=3){ for $m (@ms){ print "$m\;$k\n";}}}' | gzip > m2n5.3.gz
```

Now run training


```
import numpy as np
import gzip,collections,gensim.models.doc2vec
from gensim.models import Doc2Vec
from collections import OrderedDict, namedtuple
import multiprocessing, random,unicodedata, re
from random import shuffle
import datetime
cores = multiprocessing.cpu_count()

lmax=100000000
max_au = 5
min_ms = 10

f=gzip.open("n2msg5.10.40.gz")
tags=list()
ms=list()
nl=0
for line in f:
 l = line.rstrip().decode('ascii', 'ignore')
 i,n,e,m=l.split(';')
 m = re.sub('[^A-Za-z0-9]+', ' ', m)
 tag = n + ':' + e
 #if (len(m)>100 and len(tag)>10):
 tags.append(tag)
 ms.append(m)
 nl+=1
 if (nl>lmax): break

ldoc = namedtuple('ldoc', 'words tags split')

docs = []
for l in enumerate([ (w,t) for w,t in zip(ms, tags) ]):
 words=l[1][0].split()
 tags=l[1][1]
 split=random.choice(['test','train','validate'])
 docs.append(ldoc(words, [ tags ], split))

mod = Doc2Vec(dm=1, dm_mean=1, size=200, window=10, negative=20, hs=0, min_count=5, workers=cores) 
mod.build_vocab(docs)
train_docs = [doc for doc in docs if doc.split == 'train']
test_docs = [doc for doc in docs if doc.split == 'test']
dl = docs[:]  # for reshuffling per pass
alpha, min_alpha, passes = (0.025, 0.001, 200)
alpha_delta = (alpha - min_alpha) / passes
%cpaste
for epoch in range(passes):
 shuffle(dl)
 mod.alpha, mod.min_alpha = alpha, alpha
 mod.train(dl)
 alpha -= alpha_delta
--


#######################
a = set()
for m,n in zip(ms, tags):
 a.add(m+';'+n)

ts=list()
msg=list()
for mn in a:
 m,n = mn.split(';')
 ts.append(n)
 msg.append(m)

m=collections.Counter(msg)
n=collections.Counter(ts)
print(len(n))

mm0 = [ na for na,c in m.most_common(len(m)) if c <= max_na ]
nn0 = [ na for na,c in n.most_common(len(n)) if c >= min_ms ]
mm = set(mm0)
nn = set(nn0)
ldoc = namedtuple('ldoc', 'words tags split')

docs = []
for l in enumerate([ (w,t) for w,t in zip(msg, ts) if t in nn and w in mm]):
 words=l[1][0].split()
 tags=l[1][1]
 split=random.choice(['test','train','validate'])
 docs.append(ldoc(words, [ tags ], split))

simple_models = [ Doc2Vec(dm=1, dm_concat=1, size=200, window=5, negative=20, hs=0, min_count=2, workers=cores),
 Doc2Vec(dm=0, size=200, negative=20, hs=0, min_count=2, workers=cores),
 Doc2Vec(dm=1, dm_mean=1, size=200, window=10, negative=20, hs=0, min_count=2, workers=cores) ]
	
simple_models[0].build_vocab(docs)
print(simple_models[0])
for model in simple_models[1:]:
    model.reset_from(simple_models[0])
    print(model)

models_by_name = OrderedDict((str(model), model) for model in simple_models)
#from gensim.test.test_doc2vec import ConcatenatedDoc2Vec
#models_by_name['dbow+dmm'] = ConcatenatedDoc2Vec([simple_models[1], simple_models[2]])
#models_by_name['dbow+dmc'] = ConcatenatedDoc2Vec([simple_models[1], simple_models[0]])

train_docs = [doc for doc in docs if doc.split == 'train']
test_docs = [doc for doc in docs if doc.split == 'test']
dl = docs[:]  # for reshuffling per pass


alpha, min_alpha, passes = (0.025, 0.001, 100)
alpha_delta = (alpha - min_alpha) / passes

%cpaste
for epoch in range(passes):
 shuffle(dl)
 for name, mod in models_by_name.items():
  mod.alpha, mod.min_alpha = alpha, alpha
  mod.train()
 alpha -= alpha_delta
--


for name, mod in list(models_by_name.items())[0:3]:
 #print([ (mod.docvecs.offset2doctag[i],s) for i,s in mod.docvecs.most_similar('Ken Coar:coar@apache.org')[0:3]])
 print(name)
 print ([ (mod.docvecs.offset2doctag[i],s) for i,s in mod.docvecs.most_similar('Volker Lendecke:vl@samba.org')[0:3]])


[ (mod.docvecs.offset2doctag[i],s) for i,s in mod.docvecs.most_similar('brendan%mozilla.org:brendan%mozilla.org') ]

origin = mod.docvecs['Volker Lendecke:vl@samba.org']
word_sims = [('word', word, score) for word, score in mod.most_similar([origin],topn=20)]
tag_sims = [('tag', mod.docvecs.offset2doctag[tag], score) for tag, score in mod.docvecs.most_similar([origin],topn=20)]
results = sorted((tag_sims + word_sims),key=lambda tup: -tup[2])
results[:20]

for na in nn0:
 origin = mod.docvecs[na]
 res = [(mod.docvecs.offset2doctag[tag], score) for tag, score in mod.docvecs.most_similar([origin],topn=3) if score >.8]
 if len(res)> 1 : print(na + ";" + str (res[1]) + ";" + str(len(res))) 
```

```
pav:pav@FreeBSD.org;('pav:pav@aed309b6-a8cd-e111-996c-001c23d10e55', 0.9990030527114868);3
pav:pav@aed309b6-a8cd-e111-996c-001c23d10e55;('pav:pav@FreeBSD.org', 0.9990031719207764);3
peter:peter@FreeBSD.org;('peter:peter@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f', 0.99776291847229);2
peter:peter@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f;('peter:peter@FreeBSD.org', 0.9977630376815796);2
pfaedit:pfaedit;('George Williams:pfaedit@users.sourceforge.net', 0.9961353540420532);2
phk:phk@FreeBSD.org;('phk:phk@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f', 0.9988055229187012);2
phk:phk@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f;('phk:phk@FreeBSD.org', 0.9988054633140564);2
pipping:pipping@dcf5e38c-c701-0410-9811-92a63bff3de1;('Elias Pipping:pipping@gentoo.org', 0.9971450567245483);2
pjd:pjd@FreeBSD.org;('pjd:pjd@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f', 0.9969053268432617);2
pjd:pjd@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f;('pjd:pjd@FreeBSD.org', 0.9969053268432617);2
quasar:quasar@86ba831e-150c-0410-ab35-e06eaae15ed9;('James Haley:haleyjd@hotmail.com', 0.9985871315002441);2
reed%reedloden.com:reed%reedloden.com;('reed@reedloden.com:reed@reedloden.com', 0.9975301027297974);2
reed@reedloden.com:reed@reedloden.com;('reed%reedloden.com:reed%reedloden.com', 0.9975301027297974);2
rhijmans:rhijmans@edb9625f-4e0d-4859-8d74-9fd3b1da38cb;('johnnyw:johnnyw@ae88bc3d-4319-0410-8dbf-d08b4c9d3795', 0.9465394616127014);3
rniwa@webkit.org:rniwa@webkit.org@268f45cc-cd09-0410-ab3c-d52691b4dbfc;('commit-queue@webkit.org:commit-queue@webkit.org@268f45cc-cd09-0410-ab3c-d52691b4dbfc', 0.8360249996185303);3
roc+%cs.cmu.edu:roc+%cs.cmu.edu;("Robert O'Callahan:robert@ocallahan.org", 0.829033374786377);3
rth:rth@138bc75d-0d04-0410-961f-82ee72b054a4;('hubicka:hubicka@138bc75d-0d04-0410-961f-82ee72b054a4', 0.8099225759506226);3
ru:ru@FreeBSD.org;('ru:ru@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f', 0.9968978762626648);2
ru:ru@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f;('ru:ru@FreeBSD.org', 0.9968978762626648);2
russellm:russellm@bcc190cf-cafb-0310-a4f2-bffc1f526a37;('Russell Keith-Magee:russell@keith-magee.com', 0.9990372061729431);3
rwatson:rwatson@FreeBSD.org;('rwatson:rwatson@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f', 0.9968634247779846);2
rwatson:rwatson@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f;('rwatson:rwatson@FreeBSD.org', 0.9968634247779846);2
scott%scott-macgregor.org:scott%scott-macgregor.org;('mscott%netscape.com:mscott%netscape.com', 0.8113852143287659);2
spyderous:spyderous;('dberkholz:dberkholz', 0.8272337317466736);2
tim_one:tim_one@6015fed2-1504-0410-9fe1-9d1591cc4771;('Tim Peters:tim.peters@gmail.com', 0.9987866878509521);3
timeless%mozdev.org:timeless%mozdev.org;('gavin%gavinsharp.com:gavin%gavinsharp.com', 0.8313202857971191);2
ton:ton@954f8c5b-7b00-dc11-b283-0030488c597c;('Ton Roosendaal:ton@blender.org', 0.998985230922699);2
webchick:webchick@24967.no-reply.drupal.org;('Alex Pott:alex.a.pott@googlemail.com', 0.9729871153831482);2
wilson:wilson@138bc75d-0d04-0410-961f-82ee72b054a4;('kenner:kenner@138bc75d-0d04-0410-961f-82ee72b054a4', 0.8558920621871948);3
wolf:wolf@0785d39b-7218-0410-832d-ea1e28bc413d;('bangerth:bangerth@0785d39b-7218-0410-832d-ea1e28bc413d', 0.9483410716056824);2
wpaul:wpaul@FreeBSD.org;('wpaul:wpaul@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f', 0.9979248642921448);2
wpaul:wpaul@ccf9f872-aa2e-dd11-9fc8-001c23d0bc1f;('wpaul:wpaul@FreeBSD.org', 0.9979248642921448);2
yugui:yugui@b2dd03c8-39d4-4d8f-98ff-823fe69b080e;('nobu:nobu@b2dd03c8-39d4-4d8f-98ff-823fe69b080e', 0.9095526933670044);2
```


```
for name, mod in list(models_by_name.items())[3:5]:
 print(name)
 print ([ (mod.docvecs.models[0].offset2doctag[i],s) for i,s in mod.docvecs.models[0].most_similar('Volker Lendecke:vl@samba.org')[0:3]])
 print ([ (mod.docvecs.models[1].offset2doctag[i],s) for i,s in mod.docvecs.models[1].most_similar('Volker Lendecke:vl@samba.org')[0:3]])
```

```
Doc2Vec(dm/c,d200,n20,w5,mc2,t8)
[('Volker Lendecke:vlendec@samba.org', 0.7338752746582031), ('Simo Sorce:idra@samba.org', 0.6724783182144165), ('Jeremy Allison:jra@samba.org', 0.636491596698761)]
Doc2Vec(dbow,d200,n20,mc2,t8)
[('Volker Lendecke:vlendec@samba.org', 0.5431305766105652), ('Jeremy Allison:jra@samba.org', 0.427571564912796), ('Simo Sorce:idra@samba.org', 0.4207024574279785)]
Doc2Vec(dm/m,d200,n20,w10,mc2,t8)
[('Volker Lendecke:vlendec@samba.org', 0.8088887929916382), ('Andrew Bartlett:abartlet@samba.org', 0.7365545034408569), ('Simo Sorce:idra@samba.org', 0.734250009059906)]

dbow+dmm
[('Volker Lendecke:vlendec@samba.org', 0.5431305766105652), ('Jeremy Allison:jra@samba.org', 0.427571564912796), ('Simo Sorce:idra@samba.org', 0.4207024574279785)]
[('Volker Lendecke:vlendec@samba.org', 0.8088887929916382), ('Andrew Bartlett:abartlet@samba.org', 0.7365545034408569), ('Simo Sorce:idra@samba.org', 0.734250009059906)]
dbow+dmc
[('Volker Lendecke:vlendec@samba.org', 0.5431305766105652), ('Jeremy Allison:jra@samba.org', 0.427571564912796), ('Simo Sorce:idra@samba.org', 0.4207024574279785)]
[('Volker Lendecke:vlendec@samba.org', 0.7338752746582031), ('Simo Sorce:idra@samba.org', 0.6724783182144165), ('Jeremy Allison:jra@samba.org', 0.636491596698761)]



name, mod = list(models_by_name.items())[2]



len(mod.docvecs) 

def error(mod, doc):
 z = mod.docvecs[nn0[0]][0,:,:]
 mod.docvecs.most_similar(z[0,:])

sim=mod.docvecs.most_similar(0, topn=mod.docvecs.count)


for epoch in range(passes):
 shuffle(dl)  # shuffling gets best results
 for name, mod in models_by_name.items():


error (mod, dl)
  
  eval_duration = ''
  with elapsed_timer() as eval_elapsed:
   err, err_count, test_count, predictor = error_rate_for_model(train_model, train_docs, test_docs)
   eval_duration = '%.1f' % eval_elapsed()
   best_indicator = ' '
   if err <= best_error[name]:
    best_error[name] = err
    best_indicator = '*' 
    print("%s%f : %i passes : %s %ss %ss" % (best_indicator, err, epoch + 1, name, duration, eval_duration))


dv = train_model.docvecs[nn0[0]]
sim = train_model.docvecs.most_similar(0)

        if ((epoch + 1) % 5) == 0 or epoch == 0:
            eval_duration = ''
            with elapsed_timer() as eval_elapsed:
                infer_err, err_count, test_count, predictor = error_rate_for_model(train_model, train_docs, test_docs, infer=True)
            eval_duration = '%.1f' % eval_elapsed()
            best_indicator = ' '
            if infer_err < best_error[name + '_inferred']:
                best_error[name + '_inferred'] = infer_err
                best_indicator = '*'
            print("%s%f : %i passes : %s %ss %ss" % (best_indicator, infer_err, epoch + 1, name + '_inferred', duration, eval_duration))

    print('completed pass %i at alpha %f' % (epoch + 1, alpha))
    alpha -= alpha_delta
    
print("END %s" % str(datetime.datetime.now()))
```