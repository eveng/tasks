#include <boost/config.hpp>
#include <vector>
#include <list>
#include <boost/graph/biconnected_components.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <iterator>
#include <iostream>

namespace boost
{
  struct edge_component_t {
    enum { num = 555 };
    typedef edge_property_tag kind;
  }
  edge_component;
}

int main (int argc, char ** argv){
	using namespace boost;
	typedef adjacency_list < vecS, vecS, undirectedS,
			no_property, property < edge_component_t, std::size_t >
		> Graph;
	typedef graph_traits < Graph >::vertex_descriptor vertex_t;
	Graph G (atoi(argv[1]));
	unsigned int a, b, j = 0;
	std::cin>>a;std::cin>>b;
	while (!std::cin.eof()){
		add_edge (a, b, G);
		std::cin>>a;std::cin>>b;
		if (! ((j++)%100000000)) std::cerr << j << std::endl;
	}
	std::cerr << "done reading:" << j << std::endl;

	property_map < Graph, edge_component_t >::type component = get(edge_component, G);
	std::size_t num_comps = biconnected_components (G, component);
	std::cerr << "Found " << num_comps << " biconnected components.\n";

	std::vector<vertex_t> art_points;
	articulation_points (G, std::back_inserter (art_points));
	std::cerr << "Found " << art_points.size() << " articulation points.\n";
	for (std::size_t i = 0; i < art_points.size(); ++i) {
		std::cout << (int)(art_points[i]) << std::endl;
	}	
	graph_traits < Graph >::edge_iterator ei, ei_end;
  	for (boost::tie(ei, ei_end) = edges(G); ei != ei_end; ++ei){
    		std::cout << (int)(source(*ei, G)) << " -- " 
           			<< (int)(target(*ei, G))
            			<< " \"" << component[*ei] << "\"]\n";
	}
	return 0;
}
