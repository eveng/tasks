use strict;
use warnings;

my @spec = ("apache", "Bzrmolrest", "bzr.savannah.gnu.org", "cvs.berlios.de", "cvsoss", "cvs.savannah.gnu.org", "FLOSSmole.svn", "gitleft", "google", "HGmolrest", "launchpad.net", "othersvn", "sflist", "svn.berlios.de", "svn.savannah.gnu.org", "svn.savannah.nongnu.org");

my %match;

for my $i (@spec){
	$match{$i}++;
}

my %bad;
open A0, "gunzip -c $ARGV[1]|";
while(<A0>){
     chop();
     my @x = split(/\;/, $_, -1);
     $bad{$x[0]}++;
}

sub n2p { 
	my $n = $_[0];
	my @p=split(/\//,$n,-1);
	my $lp = pop @p;
	my $plp = pop @p;
	if (defined $match{$plp}){
		return ("$plp/$lp", (join '/', @p), $#p);
	}else{
		return ($lp, (join '/', @p, $plp), $#p+1);
	}
}

open A, "|gzip>$ARGV[0].names";
open B, "|gzip>$ARGV[0].versions";
open C, "|gzip>$ARGV[0].ids";
my (%id2num, %f2num);
my $n = 0;
my $i = 0;
my ($pa, $pi, $pf) = (-1, -1, "");


while (<STDIN>){
   chop();
   my ($id, $na) = split(/\;/, $_, -1);
   my ($v, $f, $np) = n2p ($na);
   	
   if ($f ne $pf){
      print A "$f\n";
	  $pf=$f;
      $i++;
   }
   next if (defined $bad{$id});
   next if $np < 2;
   if (!defined $id2num{$id}){
      $id2num{$id} = $i;
      print C "$id $i\n";
   }
   print B "$id2num{$id} $i\n" if $id2num{$id} != $i &&
           ($id2num{$id} != $pa || $i != $pi);
   ($pa, $pi, $pf) = ($id2num{$id}, $pi, $f);
   $n ++;
   if (!($n%1000000000)) { print STDERR "$n lines done\n";}
}
print B "".($i-1)." ".($i-1)."\n";#ensure a complete list of vertices
