use warnings;
use strict;
use File::Temp qw/ :POSIX /;

print STDERR "starting ".(localtime())."\n";
open A, "|gzip>$ARGV[0].names";
open B, "|gzip>$ARGV[0].versions";
my (%c2num, %f2num);
my $n = 0;
my $i = 0;
my ($pp, $pc) = (-1, -1);
while(<STDIN>){
  chop();
  my ($p, $c) = split(/\;/, $_, -1);
  if (!defined $c2num{$p}){
    $c2num{$p} = $i;
    print A "$p\n";
    $i++;
  }
  if (!defined $f2num{$c}){
     $f2num{$c} = $i;
     print A "$c\n";
     $i++;
  }
  print B "$c2num{$p} $f2num{$c}\n" if 
		($c2num{$p} != $pp || $f2num{$c} != $pc);
  ($pp, $pc) = ($c2num{$p}, $f2num{$c});
  $n ++;
}
print B "".($i-1)." ".($i-1)."\n";#ensure a complete list of vertices

