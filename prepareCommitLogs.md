How to prepare commit logs for text analysis
============================================


The task is to determine if do2vec algorithm can identify
an individual who may use different email/name combinations
by the similarity of their commit log messages.

Theory
------

An individual A may use A.name1,A.email1 in some commits
and A.name2,A.email2 in others. This may be because they do commits
from different repositories (e.g., laptop/server) or they may have
changed their email/name over time, or for other reasons.


We will obtain commit logs by an individual and label them with the
name,email used in the commit. Thus labeled commits will be run
through doc2vec algorithm and for each observed document label
(name1,email1) the closest other label (name2,email2) will be
obtained using the distance in the document embedding space.  The
similarity scores above .8 woould be considered as match:
(name1,email1) and (name2,email2) belong to the same individual. The
resulting matches will be analyzed through other means to determing
how many of them are likely to be correct.


Preparing data
==============


delta.id2content containes a mapping from the commit message id to
actual message:
```
0;Add some nice initial sizes to the buffers
```

m2n.gz contains a map from message ids to name/login combination
```
5091133;bigturtle88;bigturtle@i.ua;1
17657805;buildscript;buildscript@zenphoto.org;71
```
The last number is the number of times that combination occurs in
delta.idx.*.gz files.

We want to exclude messages that are are aothored by more than
five authors to ensure meaningful commit messages, not a standard
template like "Initial commit" or similar:
```
gunzip -c m2n.gz |\
 perl -e 'while(<STDIN>){chop();($i,$n,$e,$c)=split(/\;/, $_, -1);$m2n{$i}{"$n\;$e"}+=$c; $n2m{"$n\;$e"}{$i}+=$c;}while (($k,$v)=each %m2n){$na=scalar(keys %{$v});print "$k\;$na\n";}' |\
 gzip > m2na.gz
gunzip -c m2na.gz | awk -F\; '{ if ($2>5) print $1}' | sort -u > m5
```
Now m5 contains ids of such common template commit logs.

Now we can use this info to produce a subset of valid commit log
messages
```
gunzip -c m2n.gz | perl -e 'open A,"m5"; while(<A>){chop;$b{$_}++;}; while(<STDIN>){chop();($i,$n,$e,$c)=split(/\;/, $_, -1);next if defined $b{$i}; $n2m{"$n\;$e"}{$i}+=$c;}while (($k,$v)=each %n2m){@ms=(keys %{$v});if ($#ms>=9){ for $m (@ms){ print "$m\;$k\n";}}}' |\
 gzip > m2n5.10.gz
```
m2n5.10.gz now contains only messages that were used by 5 or fewer
authors, with each author writing at least ten messages.

Finally we need to include the actual text of the message for the
doc2vec analysis:
```
gunzip -c m2n5.10.gz | \
perl -e 'open A, "delta.id2content"; while(<A>){chop();($i,@m)=split(/\;/);$ms{$i}=join ";", @m;} while(<STDIN>){ chop();@x=split(/\;/);print "$_\;$ms{$x[0]}\n"if length($ms{$x[0]})>40;}' |\
 gzip > n2msg5.10.40.gz
```
We further ensure each message is at least 40 characters long (that may make seome ids have fewer than 10 messages).


Lets create the same output with min 15 character messages
```
gunzip -c m2n5.10.gz | \
 perl -e 'open A, "delta.id2content"; while(<A>){chop();($i,@m)=split(/\;/);$mm=join ";", @m;$mm=~s/NEWLINE/ /g;$ms{$i}=$mm;} while(<STDIN>){ chop();@x=split(/\;/);print "$_\;$ms{$x[0]}\n"if length($ms{$x[0]})>15;}' |\
 gzip > n2msg5.10.15.gz
```



Doc2Vec
=======
```
import numpy as np
import gzip,collections,gensim.models.doc2vec
from gensim.models import Doc2Vec
from collections import OrderedDict, namedtuple
import multiprocessing, random,unicodedata, re
from random import shuffle
import datetime
cores = multiprocessing.cpu_count()

lmax=1000000000
max_au = 5
min_ms = 10

// First read into tags and ms
f=gzip.open("n2msg5.10.40.gz")
tags=list()
ms=list()
nl=0
for line in f:
 l = line.rstrip().decode('ascii', 'ignore')
 i,n,e,m=l.split(';')
 m = re.sub('[^A-Za-z0-9]+', ' ', m)
 tag = n + ':' + e
 tags.append(tag)
 ms.append(m)
 nl+=1


ldoc = namedtuple('ldoc', 'words tags split')
docs = []
for l in enumerate([ (w,t) for w,t in zip(ms, tags) ]):
 words=l[1][0].split()
 tags=l[1][1]
 split=random.choice(['test','train','validate'])
 docs.append(ldoc(words, [ tags ], split))

mod = Doc2Vec(dm=1, dm_mean=1, size=200, window=10, negative=20, hs=0, min_count=5, workers=cores) 
mod.build_vocab(docs)
dl = docs[:] 


alpha, min_alpha, passes = (0.025, 0.001, 1000)
alpha_delta = (alpha - min_alpha) / passes
for epoch in range(passes):
 shuffle(dl)
 mod.alpha, mod.min_alpha = alpha, alpha
 mod.train(dl)
 alpha -= alpha_delta

```
Now that the training is done, we can investigate similarity among
name/email pairs.

```
for na in [ 'pav:pav@FreeBSD.org', 'spyderous:spyderous', 'tim_one:tim_one@6015fed2-1504-0410-9fe1-9d1591cc4771']:
 nav = mod.docvecs [ na ]
 res = [(mod.docvecs.offset2doctag [tag], score) for tag, score in mod.docvecs.most_similar([ nav ]) if score >.8]
 if len(res)> 1 : print(na + ";" + str (res[1:len(res)]) + ";" + str(len(res))) 
```
Produces:
```
pav:pav@FreeBSD.org;
[('pav:pav@aed309b6-a8cd-e111-996c-001c23d10e55', 0.9990030527114868),
 ('miwi:miwi@FreeBSD.org', 0.8269498944282532),
 ('miwi:miwi@aed309b6-a8cd-e111-996c-001c23d10e55', 0.8258615136146545)];4

spyderous:spyderous;
[('dberkholz:dberkholz', 0.8272337317466736)];2

tim_one:tim_one@6015fed2-1504-0410-9fe1-9d1591cc4771;
 [('Tim Peters:tim.peters@gmail.com', 0.9987866878509521),
  ('gvanrossum:gvanrossum@6015fed2-1504-0410-9fe1-9d1591cc4771', 0.8066046833992004)];3
```

m2n.gz is obtained as follows:
------------------------------
```
seq 0 33 | while read i;
 do gunzip -c m2n.$i.gz
done | perl -e 'while(<STDIN>){chop();($i,$n,$e,$c)=split(/\;/, $_, -1);$m{"$i\;$n\;$e"}+=$c;}while (($k,$v)=each %m){print "$k\;$v\n";}' | \
gzip > m2n.gz
```
Each m2n.$i.gz is produced via
```
gunzip -c delta.idx.$i.gz | perl m2n.perl | gzip > m2n.$i.gz
```
With m2n.perl being
```
my %v;
while(<STDIN>){
        chop();
        my ($i,$s,$h,$n,$e,@rest)=split(/\;/, $_, -1);
        $v{"$i\;$n\;$e"}++;
}
while (my ($x, $c) = each %v){
        print "$x\;$c\n";
}
```

