Graph databases
===============

Certain tasks may require queries that travers a network of
links. Graph databases are supposed to help.


As usual, we get the \#1 graph database neo4j deployed on a docker
container
```
sdocker run -it -p 7474:7474 -v /data/shared/neo4j:/data \
  -v /da3_data/delta/:/delta --name gh neo4j
```

We have mounted a space for the database on /data/shared/neo4
and /da3_data/delta/ to import input files.


Each database prefers its own way of handling input.
neo4j is no exception, so we need to prepare the data
via the following script:
```
use strict;
use warnings;
my (%ff, %id);
open A, "|gzip > nflinks1.csv.gz";
my ($nf, $nn) = (0,0);
print A ":START_ID(Person);:END_ID(File)\n";
while(<STDIN>){
        chop();
        s/[^[:ascii:]]//g; #otherwise the same email may appear as	several keys
        s/\|/PIPE/g; s/\`/ /g;
        my ($n,$e,$f) = split(/\;/, $_, -1);
        my $k="$n:$e";
        $id{$k} ++; $ff{$f} ++;
        print A "$k\;$f\n";
}
open A, "|gzip > nnodes1.csv.gz";
print A "cnt:int;pid:ID(Person)\n";
while (my ($k, $v) = each %id){ print A "$v\;$k\n"; }
open A, "|gzip > fnodes1.csv.gz";
print A "cnt:int;fid:ID(File)\n";
while (my ($k, $v) = each %ff){ print A "$v\;$k\n"; }
```

The resulting three files can now be, hopefully imported:
```
bin/neo4j-import --into /data/n2f.db --delimiter ';' \
 --array-delimiter '`' --quote '|' \
 --id-type string \
 --nodes /delta/nnodes1.csv.gz \
 --nodes /delta/fnodes1.csv.gz \
 --relationships:AUTHORED /delta/nflinks1.csv.gz
```


A few minutes later it seems to have concluded.

```
IMPORT DONE in 2m 52s 852ms. Imported:
  8150436 nodes
  14264585 relationships
  16300872 properties
```

Now we can connect the the databse port localhost:7474
(need to forward it to da1 or wherever the container is running).

Beforehand, we need to rename the databse (ne4j starts using
graph.db)

Now lets run some queries:
```
MATCH (n:Person)-[a:AUTHORED]->(f:File)
where
 (n.pid='Leonard H. Tower Jr.:tower@gnu.org' OR
  f.fid='hurd-and-linux.htm')
RETURN a LIMIT 25;
```

It gives the following graph after running for a long while...

![Authored](Authored1.png)

(btw, 'Leonard H. Tower Jr.:tower@gnu.org' has authored a single
file in this extract, so we are selecting him, the file, and all
authors who also modified the file.

Lets do something more sophisticated, for example developer
succession measure:
```
MATCH (dev)-[:AUTHORED]->(files)<-[:AUTHORED]-(people)
where (dev.pid='Gordon Matzigkeit:gord@fig.org' and not dev = people)
RETURN people.pid AS name, count(files.fid) AS nfiles
ORDER BY nfiles DESC
LIMIT 25;
```

It returns, coincidentally, the same person (with different
name/email combinations) in the top spots:


name|nfiles
----|------
Gordon Matzigkeit:gord@gnu.org|	6
Gordon Matzigkeit:gord@gnu.ai.mit.edu|	6
Thomas Tanner:tanner@gmx.de|	6
Gordon Matzigkeit:gord@profitpress.com|	6
Gary V. Vaughan:gary@gnu.org	6
Alexandre Oliva:oliva@dcc.unicamp.br|	6
Thomas Schwinge:tschwinge@gnu.org|	6
Marcus Brinkmann:marcus@gnu.org|	6
Thomas Tanner:tanner@ffii.org|	6
Olly Betts:olly@muscat.co.uk|	5
Alexandre Duret-Lutz:adl@gnu.org|	5
Alfred M. Szmidt:ams@gnu.org|	5
Marc-Andr Lureau:marcandre.lureau@gmail.com|	5
Reuben Thomas:rrt@sc3d.org|	4
Alexandre Oliva:oliva@lsd.ic.unicamp.br|	4
Paul Eggert:eggert@cs.ucla.edu|	4
