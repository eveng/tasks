How to eliminate problematic edges
======================================

As described in the document (graph.md) on creating connected
components of the bipartite (content IDs and project names) graph we
can obtain connected components of the massive graph and analyze each
connected component separately, avoiding the issue of the graph not
fitting into computer RAM. In addition to content id, the file path
could be used to identify files shared among projects where the
complete file path can be used as an indication that the file (and
folder) were copied from another project. Filename and project
represent another bipartite graph that can be used to induce links
among projects. 

The resulting relationships are in f2pNew.clones and c2pNew.map
and the merged graph in f2pNew.map and the merged graph in 
fc2pNew.map. As the results show, the largest component is huge:
from 11.7M projects in file-induced graph to 13.6M projects in the merged 
graph.
```
gunzip -c f2pNew.map | cut -d\; -f2 | sort | uniq -c | sort -n | tail
    533 github.com_pjq_TTAndroidClient.git
    536 github.com_cl-_papers-we-love.git
    549 github.com_sarvex_AndroidDesignPatterns.git
   1090 github.com_zge_swirl_courses.git
   2187 github.com_ywo_android-open-project.git
11734323 bks

gunzip -c c2pNew.map | cut -d\; -f2 | sort | uniq -c | sort -n | tail
    610 github.com_tt_learnxinyminutes-docs.git
    935 github.com_G041t_usb.git
   1151 github.com_nk4_practice_assignment.git
   2187 github.com_zjx_android-open-project.git
12349569 cbg


gunzip -c fc2pNew.map | cut -d\; -f2 | sort | uniq -c | sort -n | tail
    549 github.com_sarvex_AndroidDesignPatterns.git
    610 github.com_tt_learnxinyminutes-docs.git
   1151 github.com_nk4_practice_assignment.git
   2187 github.com_ywo_android-open-project.git
13677204 din
```

Obviously, it makes little sense that 14M projects would be so directly related. 
The relationship must, therefore be of a somewhat superficial nature, such as 
common templates or includes that are used in thousands of repositories. 
If so, what are these most common files and content IDs and would the size of 
the largest component go down substantially if we eliminate them?


Identifying high-degree content IDs and files
--------------------------------------------- 

First, lets identify content IDs and files that may be responsible for the explosive
connectivity. It may be that a few templates hypothesized above above are
responsible. If so, we can identify them by counting the number of projects each is connected to
and by eliminating them from the graph, would break up this huge connected component into smaller 
chunks.

Note that the connectExport.perl encodes content ID by the id of the first project and the 
links in ".versions" file are always from
the content id (on the left) to the project id (on the right). 
 
A simple sort and count would obtain the result:
```
gunzip -c f2pNew.versions | sort -u -T. | gzip > f2pNew.versions.s
gunzip -c f2pNew.versions.s | cut -d\  -f1 | sort | uniq -c | sort -rn > f2pNew.freq
```
Please note that this is not the fastest technique:
f2pNew.versions and c2pNew.versions contain 30+B links and you need a 
very fast file system to do the sort. It may take weeks on da cluster.
Spark may be a suitable technology to spread the sorting over multiple servers. 

An alternative is to use the much smaller .versions1 file produced by exportPrune.perl (see graph.md). 
Unfortunately  exportPrune.perl sorts the nodes, so the content ID may be on either side of the 
link. The following should, however work:
```
gunzip -c c2pNew.versions1 | awk '{ print $1; print $2}' | sort | uniq -c | sort -rn > c2pNew.freq
```

Ok, now we can go ahead and produce the list of nodes with high degrees:
from  10 to 100K for file-induced graph: 
```
awk '{if($1>100000) print $2}' f2pNew.freq > f2pNew.100k
awk '{if($1>10000) print $2}' f2pNew.freq > f2pNew.10k
awk '{if($1>1000) print $2}' f2pNew.freq > f2pNew.1k
awk '{if($1>100) print $2}' f2pNew.freq > f2pNew.1c
awk '{if($1>10) print $2}' f2pNew.freq > f2pNew.10
```
and from 1K to 1M for the content induced graph
```
awk '{if($1>1000000) print $2}' c2pNew.freq > c2pNew.1m
awk '{if($1>100000) print $2}' c2pNew.freq > c2pNew.100k
awk '{if($1>10000) print $2}' c2pNew.freq > c2pNew.10k
awk '{if($1>1000) print $2}' c2pNew.freq > c2pNew.1k
```

Now we can construct the new set of edges eliminating the common nodes
```
for t in 10 1c 1k 10k 100k
do  gunzip -c f2pNew.versions.s | \
 perl -e 'open A, "f2pNew.'$t'";while(<A>){chop();$b{$_}++;}while(<STDIN>){chop();($i,$j)=split(/ /);if(!defined $b{$i} && $i != $j){print "$i $j\n";}}' |\
  gzip > f2pNew.versions.s.$t
  gunzip -c f2pNew.versions.s.$t | connect 13784514 | gzip > f2pNew.clones.$t
done
```

Since the graph for the content ids is slightly different:
```
for t in 1k 10k 100k 1m
do  gunzip -c c2pNew.versions1 | \
 perl -e 'open A, "c2pNew.'$t'";while(<A>){chop();$b{$_}++;}while(<STDIN>){chop();($i,$j)=split(/ /);if(!defined $b{$i} && !defined $b{$j}){print "$i $j\n";}}' |\
  gzip > c2pNew.versions1.$t
 gunzip -c c2pNew.versions1.$t | connect 15633981 | gzip > c2pNew.clones.$t
done
```


Now the moment of truth: does our largest cluster decrease in size substantially?
```
gunzip -c f2pNew.clones.1k | cut -d\;  -f2 | sort | uniq -c | sort -n | tail
    989 64821
   1045 270987
8831273 0
```
Not really, still a very large cluster: 8M
Lets eliminate all 100+ links:

```
awk '{if($1>100) print $2}' f2pNew.freq > f2pNew.1c
gunzip -c f2pNew.versions.s | perl -e 'open A, "f2pNew.1c";while(<A>){chop();$b{$_}++;}while(<STDIN>){chop();($i,$j)=split(/ /);if(!defined $b{$i} && $i != $j){print "$i $j\n";}}' | gzip > f2pNew.versions.s.1c
gunzip -c f2pNew.versions.s.1c | connect 13784514 | gzip > f2pNew.clones.1c
gunzip -c f2pNew.clones.1c | cut -d\;  -f2 | sort | uniq -c | sort -n | tail
    268 70151
    482 35155
5575727 0
```
It seems it is hard to go below the 5M largest cluster: could it be real?
Lets eliminate 10+ links:

```
awk '{if($1>10) print $2}' f2pNew.freq > f2pNew.10
gunzip -c f2pNew.versions.s | perl -e 'open A, "f2pNew.10";while(<A>){chop();$b{$_}++;}while(<STDIN>){chop();($i,$j)=split(/ /);if(!defined $b{$i} && $i != $j){print "$i $j\n";}}' | gzip > f2pNew.versions.s.10
gunzip -c f2pNew.versions.s.10 | connect 13784514 | gzip > f2pNew.clones.10
gunzip -c f2pNew.clones.10 | cut -d\;  -f2 | sort | uniq -c | sort -n | tail
    161 15071
    188 309056
    284 326405
    465 1246
1923751 18
```

Ok, 2M is still very large, but with such a large component (now reduced), can we figure 
out what are the links that would, if removed, would break it apart the most? With the 
largest component of 2M nodes it may now be possible.

Special files
=============
What are these special nodes that link up more than a million of projects?
```
cat c2pNew.1m
1208
13800

gunzip -c c2pNew.names | head -1207 | tail -1
github.com_wanghaiquan_rails.git 

gunzip -c c2pNew.names |head -13799 | tail -1
github.com_revues_socket.io-client.git
```

These two particular projects are associated with almost 59K of content IDs
```
gunzip -c c2pNew.ids | grep -E ' 1208$| 13800$' | sort -u > c2pNew.ids.1m
wc -l c2pNew.ids.1m
58943 c2pNew.ids.1m
```

To determine individual content ids responsible for linking many files lets look at the 
direct mapping between content IDs and file names. 15 CIDs have links to 1M or more distinct filenames (including project path):
```
cat c2nf1m
308993074
322377
143546839
934
870055
12215193
162343644
2538113
332220794
2409856
162373885
162343837
154075632
154078751
16611480
```

c2nf1m is obtained from c2nf.gz which, in turn, is obtainef from  c2f.$i.gz:
```
seq 0 45| while read i; do gunzip -c c2f.$i.gz | cut -d\; -f1 | uniq -c; done | awk '{if ($1>1)print $2";"$1}' | perl -e 'while(<STDIN>){chop();($c,$n)=split(/\;/);$c2n{$c}+=$n}while(($c,$n)= each %c2n){print "$c\;$n\n"}'|gzip > c2nf.gz
gunzip -c c2nf.gz | sort -t\; -nr -k2 -T. | gzip > c2nf.s.gz
gunzip -c c2nf.s.gz | awk -F\; '{if ($2> 1000000) print $1}' > c2nf1m
```
The c2f.$i.gz are produced in turn from all.idx.$i.gz
```
gunzip -c all.idx.$i.gz |perl $cloneDir/c2f.perl | gzip > c2f.$i.gz
```

For example, the most common CID 308993074 with 5,692,821 distinct
files maps to, for example
```
gunzip -c all.idx.0.gz | grep -F 308993074 | grep -w 308993074 |head -2
308993074;44;NewNewNew1.36/github.com_abdyla_NGECore2.git/scripts/object/tangible/loot/mustafar/old_republic_tech_03.py/8f14f4e78afd029f2a1d010177778dcf83800b65
308993074;44;NewNewNew1.36/github.com_abdyla_NGECore2.git/scripts/object/building/naboo/syren4_safehouse.py/8f14f4e78afd029f2a1d010177778dcf83800b65
```
This is a tiny (44 byte file) 
```
import sys

def setup(core, object):
	return
```

Another CID 332220794 has 1,288,555   distinct
files and maps to, for example
```
gunzip -c all.idx.0.gz | grep -F 332220794 | grep -w 332220794 |head -2
332220794;781;ggitNew/a2dp-switcher.git/proguard-project.txt/0047ce954e731718d8cb520705cbcc525caf6b30

```
As hypothesized, the proguard-project.txt appears to be a standard template used by numerous projects.



Bridges and Articulation nodes
===============================
The bridge is the edge that, if removed, would the number of 
connected components. The articulation node also 
increases the number of 
connected components if removed. They may also be referred as cut edges and nodes.

Boost library has several implementations finding the articulation nodes. With the sparse graph, 
most of these articulation nodes and bridges may not break the huge component into 
pieces of equal size but are more likely  to cut a small piece of graph off. 

What are these nodes? How can we find them without an extensive search?
A simple search strategy may look as follows:
1. Find the largest connected sub-graph
2. Find articulation nodes in it
3. Iterate over articulation nodes n in the largest sub-graph
4.   delete node n and calculate the size of the largest sub-graph and associate that value with n
5. Chose n with the minimal associated value

The algorithm involves finding connected sub-graph C(NE), largest
component (N log(N)), finding articulation points of the component
O(NE), and, for each articulation point iterating to find the size of
the largest component O(N^2 log(N)). This appears to be polynomial
unless I missed some steps. A somewhat similar concept of a critical
point (defined as a node whose deletion results in the minimum
pair-wise connectivity among the remaining nodes) is an NP complete
problem. If we look for a set of m nodes whose removal minimizes the
size of the largest component that adds m! term to complexity, making
the problem exponentially hard.


Lets try:
```
gunzip -c c2pNew.versions1.1k | perl -e 'open A, "max"; while(<A>){chop();$g{$_}++;} while (<STDIN>){ chop();($a,$b)=split(/ /);if (defined $g{$a} || defined $g{$b}){ print "$a $b\n";}}' | gzip > c2pNew.versions1.1k.max
gunzip -c c2pNew.versions1.1k.max | ./articulation 15633981 | gzip > c2pNew.art
Found 2052555 biconnected components.
Found 589680 articulation points.
```

On the full graph:
```
gunzip -c c2pNew.versions1 | ./articulation 15633981 | gzip > c2pNew.art.full
Found 2174473 biconnected components.
Found 487668 articulation points.
```


