Docker swarm
=======

The da[0-3] each runs a docker deamon, but it may be inconvenient to log into each server 
to start docker containers. 

A swarm solves that problem by running containers on the group of available servers.

We need several concepts to understand the swarm:

1. Need to keep track of what containers are on what server (service discovery)
1. The servers may go down at any point, so how do we keep the state of what server is up (health checking)
1. It is unwise to keep this meta info on a single server: what if it goes down? (resilient)

Solution: distributed store (typically a key-value store)

Various options: 
 - zookeeper/dooserd/etcd - strongly-consistent but no  health checking 

consul seems to do a good job:
======

```
#on da3
consul agent -server -bootstrap-expect 1 -data-dir /data/consul -node=master -bind=$IP -client $IP
#on da[0-2]
consul agent -data-dir /data/consul -node=$HOSTNAME -bind=$IP
consul join -rpc-addr=da3:8400 $IP
consul members -rpc-addr=da3:8400
Node              Address              Status  Type    Build  Protocol  DC
da0.eecs.utk.edu  160.36.127.253:8301  alive   client  0.6.3  2         dc1
da1.eecs.utk.edu  160.36.127.252:8301  alive   client  0.6.3  2         dc1
da2.eecs.utk.edu  160.36.127.251:8301  alive   client  0.6.3  2         dc1
master            160.36.127.222:8301  alive   server  0.6.3  2         dc1
```

Now we have the distributed store, lets create a swarm 
=====================

First problem: swarm program can be run as a standalone or in a  container.

- If standalone, we need to install it on the server....
- If in the container, we can't use the same docker deamon for the cluster....
- Solution: create a virtual machine to manage the swarm
```
docker-machine create -d virtualbox swarm-master
#now start container semanage in the virtual machine swarm-master
docker-machine ssh swarm-master 'docker run -d -p 5732:2375 --name smanage swarm manage \
  nodes://160.36.127.253:2375,160.36.127.252:2375,160.36.127.251:2375,160.36.127.222:2375'
```


The swarm master VM is accessible from da3:
```
echo $(docker-machine ip swarm-master)
192.168.99.100
```

Now lets setup an alias to reduce typing
```
alias sdocker='docker -H tcp://'$(docker-machine ip swarm-master)':5732'

```
What do we have here: wow 1.6TB of RAM in the swarm!
```
sdocker info
Containers: 409
 Running: 16
 Paused: 0
 Stopped: 393
Images: 60
Role: primary
Strategy: spread
Filters: health, port, dependency, affinity, constraint
Nodes: 4
 da0.eecs.utk.edu: 160.36.127.253:2375
  └ Status: Healthy
  └ Containers: 26
  └ Reserved CPUs: 0 / 25
  └ Reserved Memory: 0 B / 396.6 GiB
  └ Labels: executiondriver=native-0.2, kernelversion=3.10.0-327.4.5.el7.x86_64, operatingsystem=Red Hat Enterprise Linux, storagedriver=devicemapper
  └ Error: (none)
  └ UpdatedAt: 2016-02-15T18:49:11Z
 da1.eecs.utk.edu: 160.36.127.252:2375
  └ Status: Healthy
  └ Containers: 31
  └ Reserved CPUs: 0 / 25
  └ Reserved Memory: 0 B / 396.6 GiB
  └ Labels: executiondriver=native-0.2, kernelversion=3.10.0-327.4.5.el7.x86_64, operatingsystem=Red Hat Enterprise Linux, storagedriver=devicemapper
  └ Error: (none)
  └ UpdatedAt: 2016-02-15T18:48:54Z
 da2.eecs.utk.edu: 160.36.127.251:2375
  └ Status: Healthy
  └ Containers: 70
  └ Reserved CPUs: 0 / 25
  └ Reserved Memory: 0 B / 396.6 GiB
  └ Labels: executiondriver=native-0.2, kernelversion=3.10.0-327.4.5.el7.x86_64, operatingsystem=Red Hat Enterprise Linux, storagedriver=devicemapper
  └ Error: (none)
  └ UpdatedAt: 2016-02-15T18:48:54Z
 da3.eecs.utk.edu: 160.36.127.222:2375
  └ Status: Healthy
  └ Containers: 282
  └ Reserved CPUs: 0 / 8
  └ Reserved Memory: 0 B / 396.5 GiB
  └ Labels: executiondriver=native-0.2, kernelversion=3.10.0-327.4.5.el7.x86_64, operatingsystem=Red Hat Enterprise Linux, storagedriver=devicemapper
  └ Error: (none)
  └ UpdatedAt: 2016-02-15T18:48:54Z
Plugins: 
 Volume: 
 Network: 
Kernel Version: 4.1.17-boot2docker
Operating System: linux
Architecture: amd64
CPUs: 83
Total Memory: 1.549 TiB
Name: 43974d55a94b
```

Overlay networks
====

- How do containers communicate among themselves?

- It turns out its possible to create an overlay network for all containers in the swarm

```
sdocker network create -d overlay --subnet=10.0.1.0/24 danet
```

- Now check it out
```
#start containers on different nodes
sdocker run -itd --name shell1 --net danet alpine /bin/sh
sdocker run -itd --name shell1 --net danet -e constraint:node!=da0.eecs.utk.edu alpine /bin/sh
sdocker ps | grep shell[12]
732d51b5eafb        alpine                       "/bin/sh"     da0.eecs.utk.edu/shell2
0bbc2230f65a        alpine                       "/bin/sh"     da1.eecs.utk.edu/shell1
```

Are they connected?
```
sdocker exec -it shell2 /bin/sh
/ # ping shell1
PING shell1 (10.0.1.2): 56 data bytes
64 bytes from 10.0.1.2: seq=0 ttl=64 time=0.276 ms
```
Voila!


