How to create an openmpi cluster
================================

Tor example to be able to run graph partitioning using ram of all
four servers.

* First create an overlay network for these containers
```
sdocker network create -d overlay --subnet=10.0.2.0/24 mpi
```

* Now start four containers

```
  seq 0 3 | while read i
  do sdocker run  -d \
    -v /da3_data/delta:/data \
    --hostname="mpi$i" \
    --name=mpi$i \
    --net mpi -e constraint:node==da$i.eecs.utk.edu \
	audris/openmpi /bin/init.sh
  done
```
