The story of left-pad
=====================

The code concerned is simple and brief
```
module.exports = leftpad;

function leftpad (str, len, ch) {
str = String(str);

var i = -1;

if (!ch && ch !== 0) ch = ' ';

len = len - str.length;

while (++i < len) {
str = ch + str;
}

return str;
}
```

The story of it being taken out of NPM is: 
[here](http://www.theregister.co.uk/2016/03/23/npm_left_pad_chaos/)
 


How to obtain the global left-pad supply chain network:
Lets start from the files 
```
seq 0 47 | while read i; do gunzip -c c2f.$i.s; done | grep -w left-pad | gzip > c2f.s.left-pad
gunzip -c  c2f.s.left-pad | head 
621508377;index.js/left-pad/node_modules/line-numbers/node_modules/babel-core/node_modules/github.com_iliakan_javascript-nodejs.git;1
...
```

That's is the initial seed listing all left-pad modules in all repositories.
We can see where do the extracted files and content IDs are referring to:
```
gunzip -c c2f.s.left-pad | cut -d\; -f1 | sort -u > left-pad.cids
gunzip -c c2f.s.left-pad | cut -d\; -f2 |sed 's"/github.com_.*"";' | sort -u > left-pad.files
```
How that we have the list of files (with paths) and content IDs, we
can search for the second generation links:
```
gunzip -c c2f.0-47.s2 |grep -Ff left-pad.files > c2f.0-47.s2.left-pad.files
gunzip -c c2f.0-47.s2 |grep -Fwf left-pad.cids > c2f.0-47.s2.left-pad.cids
```

We may also get the complete info on commits. First put filenames in
their regular order:
``` 
cat left-pad.files | \
 perl -ane 'chop();@x=split(/\//,$_,-1);@y=reverse @x; print "".(join "/",@y)."\n";'\
  > left-pad.filesr
```
Then:
```
gunzip -c all.idx.[0-9]*.gz | grep -Ff left-pad.filesr | gzip > all.idx.left-pad.gz
gunzip -c all.idx.left-pad.gz | awk -F\/ '{print $NF}' | sort -u > left-pad.hash
gunzip -c delta.idx.[0-9]*.gz | grep -Ff left-pad.hash | gzip > delta.idx.left-pad.gz
```

Thus we have all commits for the second generation in
all.idx.left-pad.gz and delta.idx.left-pad.gz


We may continue to the third generation using common hashes or cids, for example, by:
```
gunzip -c all.idx.[0-9]*.gz | grep -Ff left-pad.hash | gzip > all.idx.left-pad.hash.gz

gunzip -c all.idx.left-pad.gz | awk -F\; '{print $1}' | sort -u > all.idx.left-pad.cids
gunzip -c all.idx.[0-9]*.gz | grep -Fwf all.idx.left-pad.cids | gzip > all.idx.left-pad.cids.gz
```

However, the cid- and file-based file-to-file mapping is already
available in c2fBase.map (see [file-to-file.md](https://bitbucket.org/eveng/tasks/src/master/file-to-file.md) 

```
gunzip -c c2fBase.map | grep -w left-pad > c2fBase.map.left-pad
#get canonical names
cut -d\; -f2 c2fBase.map.left-pad | sort -u > c2fBase.map.left-pad.c
#now full list in the connected sub-graph
gunzip -c c2fBase.map | grep -Ff c2fBase.map.left-pad.c > c2fBase.map.left-pad.full
```

How much stuff is there?
```
wc c2fBase.map.left-pad.c  c2fBase.map.left-pad.full
   164    167  23400 c2fBase.map.left-pad.c
  5203   5514 741152 c2fBase.map.left-pad.full
```

Now we can get the developer/commit info for these:
```
cut -d\; -f1 c2fBase.map.left-pad.full | sort -u > left-pad.flist
cat left-pad.flist | \
 perl -ane 'chop();@x=split(/\//,$_,-1);@y=reverse @x; print "".(join "/",@y)."\n";'\
  > left-pad.flistr

gunzip -c all.idx.[0-9]*.gz | grep -Ff left-pad.filesr | gzip > all.idx.left-pad.flist.gz
gunzip -c all.idx.left-pad.flist.gz | awk -F\/ '{print $NF}' | sort -u > left-pad.flist.hash
gunzip -c delta.idx.[0-9]*.gz | grep -Ff left-pad.hash | gzip > delta.idx.left-pad.flist.gz
```

Voila, the data is now in delta.idx.left-pad.flist.gz and
all.idx.left-pad.flist.gz.

