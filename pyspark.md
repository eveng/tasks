How to run spark on the created docker swarm?
=============================================

1. First run all the necessary parts

  * Begin with spark-master: I put it on da3. Note the shared network for the docker swarm: danet
  also /export/data/play is specific to each server while /export/data/shared/ipython is NFS cross-mounted
```
  sdocker run  -d --hostname="spark-master" --net danet -v /export/data/play:/data \
   -v /export/data/shared/ipython:/ipython \
   --name=spark-master -e constraint:node==da3.eecs.utk.edu lab41/spark-master
```
  * Now run the four workers
```  
  seq 0 3 | while read i
  do sdocker run  -d --env "SPARK_MASTER=spark://spark-master:7077" \
    -v /export/data/shared/ipython:/ipython \
    -v /export/data/play:/data --hostname="spark-worker" \
    --name=spark-worker$i \
    --net danet -e constraint:node==da$i.eecs.utk.edu lab41/spark-worker
  done
```
  * Finally run the pyspark client to be able to provide ipython interface. Port 8888 on da3 loopback interface 
  is used to communicate 
```
  sdocker run  -d -p 8888:8888 --hostname="da3.eecs.utk.edu" --net danet -v /export/data/play:/data \
   -v /data/shared/ipython:/ipython --env "SPARK_MASTER=spark://spark-master:7077" \
   --name=spark-client -e constraint:node==da3.eecs.utk.edu lab41/spark-client-ipython
```
1. Now you choose one of the following 

  * login to the client and run pyspark on the command line
```
  sdocker exec -it spark-client /bin/bash
  #Once inside
  /usr/local/spark/bin/pyspark --master spark://spark-master:7077
  #then type commands in the pyspark shell
```
  * Forward your laptop port to 127:0.0.1:8888 on da3 and use
  your ipython notebook at localhost:8888 to play