Results of graph construction
=============================


All the graph are constructed in order to obtain equivalence classes
of related entities. The basic input is bi-partite graph with two
types of nodes:

1. Nodes that will be eliminated when constructing equivalence
   classes, such as content IDs
2. Nodes that will remain, such as projects, files, or names


Lets introduce some notation:
1. F = {f} - entities over which the equivalence classes will be constructed
2. C = {c} - entities that will be used to link entities in F

The bipartite graph has links between c and f if, for example c is
content id of a specific version of file f. If c is linked to more
than one file, e.g., f_1 and f_2,  it induces the relationship
between them f_1 ~ f_2. The transitive closure of such relationships
corresponds to connected subsets of f in F.

Hash-to-hash cid-induced graph
-----------------------------

Here are the steps used to produce it
```
ssh da0
cd /data/play/Graphic_authorship
gunzip -c Id.hash.da[02].gz | perl /da3_data/delta/connectExport.perl /data/play/id2hash

cd /data/play
#remove duplicate links
gunzip -c id2hash.versions | perl -ne 'if (!defined $x{$_}) { print $_; $x{$_} = 1; }' |\
 gzip > id2hash.versions1

#identify non-hash IDs
gunzip -c id2hash.names | perl -e '$n=0;while(<STDIN>){chop();print "$n\n" if length($_) != 40;$n++;' | gzip > id2hash.bad

#remove non-hash nodes from the list of links
gunzip -c id2hash.versions1 | \
  perl -e 'open A, "gunzip -c id2hash.bad; while(<A>){chop();$bad{$_}++;}while(<STDIN>){chop();($a,$b)=split(/ /);print "$_\n"
   if !(defined($bad{$b}) || defined ($bad{$a});'
 | gzip > id2hash.versions3

#connect the graph
gunzip -c id2hash.versions3 | ~/bin/connect | gzip > id2hash.clones

#remap hash ids to original values
connectImport.perl id2hash | gzip > id2hash.map

#calculate distribution of the cluster size
gunzip id2hash.map | cut -d\; -f2 | sort -T. | uniq -c |\
 sort -T. -rn | gzip >  id2hash.map3.cnt


#There is still a huge cluster:
gunzip -c id2hash.map3.cnt | head
22154685 256e5ad0ec8f53c4b84d5802ce81c3a6337c9063
   3352 1892c3dce7bd8881b8a875ef668679b7e019895c
   2646 dd70cdaafb97db700c008e72aeddcab971a7053f
   2304 f955a0525249be9d3ae023acd983643e2ae9ccf8
```

But there are no invalid hashes.
The huge cluster may be caused by small files being reused among projects

To fix it, perhaps it may be worth running
gunzip -c Id.hash.da[02].gz | perl /da3_data/delta/cE1.perl /data/play/id2hash /da3_data/delta/c2s.gz
where c2s.gz lists all content ids that are shorter than 200 bytes long.
Then repeating the remaining steps as described in
[file-to-file.md](https://bitbucket.org/eveng/tasks/src/master/file-to-file.md).


Project-to-Project cid-induced graph
-----------------------------

Similar to the description of hash-to-hash mapping above. The result
is in c2pNew1.map and f2pNew1.map.



Using the mapping in the analysis
======================================

The purpose of creating equivalence classes was to
group files of a single origin. This helps understand
how the code spreads, who has expertise on what, etc.

How do we apply the created graph in the analysis?

One way to do that is to use the map to transform the original data
replacing the file name by the canonical file name representing the
equivalence class.

In particular, if we take c2fBase.map, we want to apply to all input
data all.idx.XX.gz (XX is in the range of 0-47:
```
gunzip -c all.idx.0.gz | perl  useCFmap.perl c2fBase.map | gzip > all.idx.0.CF.gz
```
The useCFmap.perl first transforms the filename to remove repository
name and other unnecessary aspects, then outputs the canonical name
and the remaining attributes, as in the following:
Before:
```
gunzip -c all.idx.1.gz| head -2
237658803;15079;NewNewNew1.55/github.com_1Jerem_usergrid.git/sdks/android/doc/org/usergrid/android/client/class-use/Client.Query.html/f3079b8dde67cf426377d20c522d4b92521efe7a
237658760;54915;NewNewNew1.55/github.com_1Jerem_usergrid.git/sdks/android/doc/org/usergrid/android/client/response/ApiResponse.html/f3079b8dde67cf426377d20c522d4b92521efe7a
```

After

```
gunzip -c all.idx.1.CF.gz| head

237658803;15079;f3079b8dde67cf426377d20c522d4b92521efe7a;clientQuery.c/src/shared_client;github.com_1Jerem_usergrid.git;NewNewNew1.55
237658760;54915;f3079b8dde67cf426377d20c522d4b92521efe7a;ApiResponse.html/response/client/android/usergrid/org/doc/android/sdks;github.com_1Jerem_usergrid.git;NewNewNew1.55
```

File to project map
===================
There may be a need to gather all files in a project.
For each all.idx.XX.gz a summary f2p.XX.gz is first produced.

```
seq 0 46 | while read i
do gunzip -c all.idx.$i.gz | perl f2pr.perl | gzip > f2p.$i.gz
done
```
Then a simple sort:

```
seq 0 46 | while read i; do time gunzip -c f2p.$i.gz | lsort 10G -t\; -k2 -k1 | gzip > f2p.$i.s2; done
```
and merge
```
time gunzip -c f2p.0.s2| lsort 120G -t\; -k2 -k1 --merge - \
<(gunzip -c f2p.1.s2) <(gunzip -c f2p.2.s2) <(gunzip -c f2p.3.s2) <(gunzip -c f2p.4.s2) \
<(gunzip -c f2p.5.s2) <(gunzip -c f2p.6.s2) <(gunzip -c f2p.7.s2) <(gunzip -c f2p.8.s2) \
<(gunzip -c f2p.10.s2) <(gunzip -c f2p.11.s2) <(gunzip -c f2p.12.s2) <(gunzip -c f2p.13.s2) \
<(gunzip -c f2p.14.s2) <(gunzip -c f2p.15.s2) <(gunzip -c f2p.16.s2) <(gunzip -c f2p.17.s2) \
<(gunzip -c f2p.18.s2) <(gunzip -c f2p.19.s2) <(gunzip -c f2p.20.s2) <(gunzip -c f2p.21.s2) \
<(gunzip -c f2p.22.s2) <(gunzip -c f2p.23.s2) <(gunzip -c f2p.24.s2) <(gunzip -c f2p.25.s2) \
<(gunzip -c f2p.26.s2) <(gunzip -c f2p.27.s2) <(gunzip -c f2p.28.s2) <(gunzip -c f2p.29.s2) \
<(gunzip -c f2p.30.s2) <(gunzip -c f2p.31.s2) <(gunzip -c f2p.32.s2) <(gunzip -c f2p.33.s2) \
<(gunzip -c f2p.34.s2) <(gunzip -c f2p.35.s2) <(gunzip -c f2p.36.s2) <(gunzip -c f2p.37.s2) \
<(gunzip -c f2p.38.s2) <(gunzip -c f2p.39.s2) <(gunzip -c f2p.40.s2) <(gunzip -c f2p.41.s2) \
<(gunzip -c f2p.42.s2) <(gunzip -c f2p.43.s2) <(gunzip -c f2p.44.s2) <(gunzip -c f2p.45.s2) \
<(gunzip -c f2p.46.s2) | gzip > f2p.0-46.s2
```

Cid to file map
===================
There may be a need to gather all files for a content id or all
content ids for each file.

For each all.idx.XX.gz a summary c2f.XX.gz is first produced.
```
seq 0 46 | while read m
do gunzip -c all.idx.$m.gz |perl $cloneDir/c2f.perl | gzip > c2f.$m.gz
done
```
Then, as above, it is sorted by cid and also by file name:
```
seq 0 46 | while read i; do gunzip -c c2f.$i.gz |\
 LC_ALL=C LANG=C sort -t\; -T. -S 30G  --compress-program=gzip -k1 -k2 | \
   gzip > c2f.$i.s;
done
seq 0 46 | while read i; do gunzip -c c2f.$i.gz |\
 LC_ALL=C LANG=C sort -t\; -T. -S 30G  --compress-program=gzip -k2 -k1 | \
   gzip > c2f.$i.s2;
done
```

Afterwards, they are merged:
```
time gunzip -c c2f.0.s2| LC_ALL=C LANG=C sort -t\; -T. -S 100G -k2 -k1 --merge - \
<(gunzip -c c2f.1.s2) <(gunzip -c c2f.2.s2) <(gunzip -c c2f.3.s2) <(gunzip -c c2f.4.s2) \
<(gunzip -c c2f.5.s2) <(gunzip -c c2f.6.s2) <(gunzip -c c2f.7.s2) <(gunzip -c c2f.8.s2) \
<(gunzip -c c2f.10.s2) <(gunzip -c c2f.11.s2) <(gunzip -c c2f.12.s2) <(gunzip -c c2f.13.s2) \
<(gunzip -c c2f.14.s2) <(gunzip -c c2f.15.s2) <(gunzip -c c2f.16.s2) <(gunzip -c c2f.17.s2) \
<(gunzip -c c2f.18.s2) <(gunzip -c c2f.19.s2) <(gunzip -c c2f.20.s2) <(gunzip -c c2f.21.s2) \
<(gunzip -c c2f.22.s2) <(gunzip -c c2f.23.s2) <(gunzip -c c2f.24.s2) <(gunzip -c c2f.25.s2) \
<(gunzip -c c2f.26.s2) <(gunzip -c c2f.27.s2) <(gunzip -c c2f.28.s2) <(gunzip -c c2f.29.s2) \
<(gunzip -c c2f.30.s2) <(gunzip -c c2f.31.s2) <(gunzip -c c2f.32.s2) <(gunzip -c c2f.33.s2) \
<(gunzip -c c2f.34.s2) <(gunzip -c c2f.35.s2) <(gunzip -c c2f.36.s2) <(gunzip -c c2f.37.s2) \
<(gunzip -c c2f.38.s2) <(gunzip -c c2f.39.s2) <(gunzip -c c2f.40.s2) <(gunzip -c c2f.41.s2) \
<(gunzip -c c2f.42.s2) <(gunzip -c c2f.43.s2) <(gunzip -c c2f.44.s2) <(gunzip -c c2f.45.s2) \
<(gunzip -c c2f.46.s2) <(gunzip -c c2f.47.s2) | gzip > c2f.0-47.s2
```
Similarly c2f.s is produced. For convenience, it may be worth
splitting c2f.s2 into small (1B line) chunks. We try to keep
everything compressed all the time, however:
```
gunzip -c c2f.0-47.s2 | \
  split -l 1000000000 --filter='gzip > $FILE.gz' --numeric-suffixes=0 - c2f.0-47.s2. 
```
This produces c2f.0-47.s2.XX.gz where XX is in 00 .. 36 (determined
by the number of lines).


