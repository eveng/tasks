How to produce file+content id equivalence classes?
==================================================

In order to determine the supply chain of software, it is necessary
to identify instances of copy or reuse of files and projects. Their
may be several ways to identify such copy/reuse, among them by
identifying identical content of files in two distinct project and
another is by identifying file pathname that is the same in
unrelated projects. Why? The basic premise is that one is unlikely
to arrive by randomly typing with the same string. It is "a monkey typing
all the works of Shakespeare" situation, where long strings are
unlikely to be reproduced.


Lets introduce some notation:
1. f - file
2. c(f, v) - the string representing the content of version v of file f
3. n(f, p) - the string representing the full path of the file f in
   project p.


The input data is in the form of code commits where for each version
of each file we have c(f_i,v_k), and, of course, n(f_i, p_l)

We declare two files f_i and f_j directly related iff there exist
versions v_k_i, v_k_j such that  c(f_i,v_k_i) = c(f_j,v_k_j) or
there exist projects p_l_i, p_l_j such that  n(f_i,p_l_i) =
c(f_j,p_l_j).

The transitive closure of the "directly related" relationship will be
called "related".

The approach to obtain such relationship can be accomplished as
follows:
```
gunzip -c c2f.0-47.s2 | perl makeCFmap.perl c2fBase c2s.gz
```
The inputs are the cid-file pairs sorted by filename (c2f.0-47.s2)
and the list of cids that correspond to file content being less
than 200 bytes. This is needed to exclude short files that happened
to be produced as identical independently.

The output is produced in three files: c2fBase.names,
c2fBase.version, and c2fBase.ids as described in [graph.md](https://bitbucket.org/eveng/tasks/src/master/graph.md).

The code starts by reading in the "bad" cids. It also introduces
a special function to separate the parts of the recorded path into
file path and project string.

As described in [graph.md](https://bitbucket.org/eveng/tasks/src/master/graph.md), the link file ".versions" is cleaned by
removing duplicate links
```
gunzip -c c2fBase.versions | \
  perl -ne 'if (!defined $x{$_}) { print $_; $x{$_} = 1; }$n++;print STDERR "$n\n" if(!($n%1000000000));' \
  | gzip > c2fBase.versions1
```

Boost library is used to find connected components (to get from
directly-related to its transitive closure)
```
gunzip -c c2fBase.versions1 | ~/bin/connect | gzip > c2fBase.clones
```

Finally, the indexes are replaced by original strings in
```
gunzip -c c2fBase.clones | \
 perl /data/delta/connectImport.perl c2fBase | \
 gzip > c2fBase.map
```

The largest cluster, however, is huge
```
gunzip -c c2fBase.map | cut -d\; -f2 | sort -T. | uniq -c |\
 sort -T. -rn | gzip >  c2fBase.map.cnt
gunzip -c c2fBase.map.cnt | head
19914179 r.js
 574005 GPL.md
 483172 djangojs.po
 368534 G2.sh
 127719 90_union.c
 126504 LICENSE.txt/lodash
 116499 selectable.css/ui/css
 114536 progress.css/extra
 107881 gemach.html
  98601 Newtonsoft.Json.xml
```

Many of these may be actual copies, e.g., of a GPL license. but we
can try to eliminate them by simply excluding such gighly conneted
nodes. Which nodes are highly connected? Lets get ones with more than 1k files. 
```
gunzip -c c2fBase.versions1.cnt | \
 awk '{ if ($1 > 1000) print $2}' > c2fBase.1k
```

No we can filter the graph by removing these nodes:
```
gunzip -c c2fBase.versions1 | \
  perl -e 'open A, "c2fBase.1k"; while(<A>){chop();$bad{$_}++;}while(<STDIN>){chop();($a,$b)=split(/ /);print "$_\n" if !(defined($bad{$b}) || defined ($bad{$a}))}'|\
 gzip > c2fBase.versions.1k

gunzip -c c2fBase.versions.1k | ~/bin/connectShort | gzip > c2fBase.clones.1k
gunzip -c c2fBase.clones.1k | perl /da3_data/delta/cI.perl c2fBase.names | gzip > c2fBase.map.1k
gunzip -c c2fBase.map.1k | cut -d\; -f2 | lsort 20G | uniq -c | lsort 10G -rn  | gzip >  c2fBase.map.cnt.1k
gunzip -c c2fBase.map.1k.cnt | head
```
The largest cluster is half the size now:

```
gunzip -c c2fBase.clones.1k.s1.cnt | tail
  54766 9056803
  55325 14750815
  60435 21938358
  62209 45266
  70430 15088312
  71739 585254
 134631 5363
 507605 183548023
 790968 4443
9027553 21
```

Where the largest cluster is css file, e.g,
bootstrap-image-gallery.min.css/lib/github.com_rliffredo_luigi.liffredo.git

