Learning objective
==================

Overall: build an extensible, self-learning, big data system applying
various technologies from cloud and social domains.

Infrastructure technology-related tasks
---------------------------------------

Big data:

-   Constructing code reuse graphs: obtaining topological properties

-   Constructing Code-Developer graphs

Bad/Missing data:

-   Entity matching

-   Outlier detection

-   Extracting successors (mentor-follower relationship)

-   Obtaining developer groups

Statistical/ML models

-   Outlier detection

-   Experimentation (e.g., A/B testing)

-   Identity matching algorithms

Presentation infrastructure

-   big data

-   bad data

-   debugging matching/outlier/grouping algorithms

Implementation of use cases
---------------------------

-   Starting a project and recruiting other developers

    -   Want with similar interests

    -   But also with relevant skills (not just the amount, but also the
        > type, e.g., language / framework)

    -   Perhaps, dissatisfied with current project


-   Truck Factor ignoring project boundaries (Mohammad)

    -   look at the full supply chain, for example, Prj A uses jQuery
        > etc

-   Relationships among projects

    -   Code reuse via copy

    -   Via inclusion (python/perl modules)

    -   CDN

    -   debian package dependencies

    -   pip package deps

    -   

Relationship matching across sources

-   Twitter follower graph homeomorphism to reddit/github follower graph

-   Homeomorphism of succession graph to follower graph

-   Relationship graph matching to detect sybil attacks

-   Relationship homeomorphism as identity matching

Reputation/ratings systems

-   Star stuff is really not based on either good theoretical or
    > empirical basis

    -   Forking

    -   Watching

    -   code copy

    -   following

    -   number of commits

    -   number of contributors

    -   diversity (distribution of commits over developers)


-   user input driven schemes

    -   learn from slashdot

    -   from stack exchange

-   Data driven schemes

    -   if you watch lots of projects perhaps the link has less impact
        > than someone who follows few

    -   if you are influential that may count for more

Data sources: 
--------------

All FLOSS VCS

-   Commits: repo/path/file/version;author;date;commit log message -
    > 10TB

-   Contentid -&gt; repo/path/file/version - 10TB

GitHub/BB social data:

-   Stars/watchers/followers

Twitter

-   Time slice (9TB)

Redit

StackExchange

Platform
--------

-   Please email me your netid and public ssh key

-   4 Servers with 1.5TB RAM + 200TB HDD (da\[0-3\])

-   Spark docker containers

-   Possibly AWS / Newton Cluster (Batch) / Beacon and Darter at JICS

Applications
------------

-   Truck factor analysis of risk

-   Code centrality

    -   developer centrality, lots of experience, therefore important

    -   end user centrality (used by people CFDs)

-   Developer movement

    -   Why they leave

    -   Where they go

    -   Is there a burnout?

-   Fault lines

    -   (see recent NY times article about Bitcoin)

-   Malware creation

    -   **Trojan/other malware discovery and spread**



