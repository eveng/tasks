use strict;
use warnings;
use Compress::LZF;

my @spec = ("apache", "Bzrmolrest", "bzr.savannah.gnu.org", "cvs.berlios.de", "cvsoss", "cvs.savannah.gnu.org", "FLOSSmole.svn", "gitleft", "google", "HGmolrest", "launchpad.net", "othersvn", "sflist", "svn.berlios.de", "svn.savannah.gnu.org", "svn.savannah.nongnu.org");

my %match;
for my $i (@spec){
	$match{$i}++;
}

sub n2p { 
	my $n = $_[0];
	my @p=split(/\//,$n,-1);
	my $lp = pop @p;
	my $plp = pop @p;
	if (defined $match{$plp}){
		return ("$plp/$lp", (join '/', @p), $#p);
	}else{
		return ($lp, (join '/', @p, $plp), $#p+1);
	}
}

my %map;
my $nl = 0;
open A, "gunzip < $ARGV[0]|";
while (<A>){
   chop();
   my ($n, $cn) = split(/\;/, $_, -1);
   if ($n ne $cn){
	  my $zn = compress ($n);
	  my $zcn = compress ($cn);
      $map{$zn} = $zcn;
   }
   $nl ++;
   if (!($nl%1000000000)) { 
	print STDERR "$nl map done\n";
   }
}


$nl = 0;
while (<STDIN>){
   chop();
   my ($id, $sz, $rest) = split(/\;/, $_, -1);
   $rest =~ s|^/||;
   my ($inputFile, $repo, $f, $v) = clean ($rest);
   my $zf = compress ($f);
   if (defined $map{$zf}){
      my $zcf = $map{$zf};
	  $f = decompress $zcf;
   }
   print "$id\;$sz\;$v\;$f\;$repo\;$inputFile\n";
   $nl ++;
   if (!($nl%1000000000)) { print STDERR "$nl lines done\n";}
}

sub clean {
   chop();
   my $rest = $_[0];
   $rest =~ s|^/||;
   my ($inputFile, $repo, @pv) = split(/\//, $rest, -1);
   if ($repo eq ""){
		print STDERR "empty repo at $_\n";
		my $v = pop @pv;
		my $f = join '/', reverse @pv;
		return ($inputFile, $repo, $f, $v);
	}
	if ($inputFile =~ /^sflist\.[0-9]/){
		$repo = "sflist/$repo";
	}
	if ($inputFile =~ /^ggitNew/){
		$repo = "google/$repo";
	}
        if ($repo =~ /^google2[0-9]*|google\.hg|google\.[0-9]*/){
		if ($pv[0] eq "svn"){
			shift @pv;
		}
                $repo = join '/', ('google', shift @pv);
        }
	if ($repo =~ /^(hg.bitbucket.com|git.bitbucket.com)/){
		$repo = shift @pv;
	}
	if ($repo =~ /^Gitmolrest/){
		$repo = shift @pv;
	}
        if ($repo =~ /^(HGmolrest|Bzrmolrest)\.[0-9]*/){
                $repo = join '/', ($1, shift @pv, $pv[0]);
		if ($pv[0] =~ /^hg.berlios.de_/){
			shift @pv;
		}
        }
	if ($repo eq "apache"){
		$repo = join '/', ("apache", shift @pv, shift @pv);
	}
	if ($repo =~ /^(bzr.savannah.nongnu.org|bzr.savannah.gnu.org|cvs.savannah.gnu.org|cvs.savannah.nongnu.org|svn.savannah.gnu.org|svn.savannah.nongnu.org|svn.berlios.de|cvs.berlios.de|hg.berlios.de)/){
		$repo = join '/', ($1, shift @pv);
		if ($repo =~ /^(svn.berlios.de|cvs.berlios.de|hg.berlios.de)/){
			shift @pv;
		}
	}
        if ($repo =~ /^(FLOSSmole.svn|launchpad.net|othersvn|gitleft|cvsoss|svnoss|hgoss)/){
                $repo = join '/', ($1, shift @pv);
        }

	if ($pv[0] eq "hg.netbeans.org"){
		shift @pv;
	}
	if ($pv[0] =~ /^bitbucket.org_/){
		shift @pv;
	}
	my $v = pop @pv;
	my $f = join '/', reverse @pv;
	return ($inputFile, $repo, $f, $v);
}
 
