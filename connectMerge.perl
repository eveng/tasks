use warnings;
use strict;
use File::Temp qw/ :POSIX /;

print STDERR "starting ".(localtime())."\n";
my $tmp = defined $ENV{TMP} ? File::Temp->new(DIR=>$ENV{TMP}) : tmpnam();
open A, "|gzip>$tmp.names";
open B, "|gzip>$tmp.versions";
my (%f2num);
my $i = 0;
my ($pa, $pb) = (-1, -1);
while(<STDIN>){
	chop();
	my ($id1, $id2) = split(/\;/, $_, -1);
	for my $v ($id1, $id2){
	   if (!defined $f2num{$id1}){
	      $f2num{$v} = $i+0;
		  print A "$v\n";
		  $i++;
	   }
	}
	print B "$f2num{$id1} $f2num{$id2}\n" if $f2num{$id1} != $f2num{$id2} &&
		($f2num{$id1} != $pa || $f2num{$id2} != $pb);
	($pa, $pb) = ($f2num{$id1}, $f2num{$id2});
}
print B "".($i-1)." ".($i-1)."\n";#ensure a complete list of vertices
undef %f2num;

close (B);
close (A);
print STDERR "finished encoding ".(localtime())."\n";
system ("gunzip < $tmp.versions | $ENV{HOME}/bin/connect |gzip > $tmp.clones");
print STDERR "finished connect ".(localtime())."\n";


my @num2f;
open A, "gunzip < $tmp.names|";
while (<A>){
	chop($_);
	push @num2f, $_;
}

open B, "gunzip < $tmp.clones|";
my $cn = "";
my %cluster = ();
while (<B>){
	chop();
	my ($f, $cl) = split(/\;/, $_, -1);
	$f=$f+0;$cl=$cl+0;
	$cluster{$cl}{$f}++;
}

while (my ($k, $v) = each %cluster){
	my %fs = ();
	for my $f (keys %{$v}){
		$fs{$num2f[$f]}++;
	}
	output (\%fs);
}
undef @num2f;

#once everything works out remove temps
unlink "$tmp.versions";
unlink "$tmp.names";
unlink "$tmp.clones";
print STDERR "finished all ".(localtime())."\n";

sub output {
	my $cl = $_[0];
	my @fs = sort { length($a) <=> length($b) } (keys %{$cl});
	for my $i (0 .. $#fs){
		print "$fs[$i]\;$fs[0]\n";
	}
}	
