Scripts to obtain connected components
======================================

One of the issues with perl and python is that they are slow for
certain tasks, such as iteration.


Many graph algorithms are all about various iterations. To create 
connected subgraphs we can use Boost library implementation
'connected_components' (see file connect.cpp).


Two-type vertex (bipartite) graph
--------------------
To do that we will read a graph that has two types of vertices:
 - cid - used as an implicit link between regular vertexes that share it
 - regular vertexes

For example we can create a connected subset via connectPbyCID.perl:
```
  gunzip -c /da3_data/delta/cid2pr.[0-1].gz | \
   perl /da3_data/delta/connectPbyCID.perl | \
   gzip > /da3_data/delta/cid2pr.0-1.clone.gz
```

uses input in cid2pr.xx.gz:
```
46386889;github.com_raichoo_illumos-gate.git;1
320421074;github.com_aalmegaard_superslides.git;1
320421074;github.com_1bigidea_superslides.git;1
```
The cid 320421074 links regular vertices
github.com_aalmegaard_superslides.git and
github.com_1bigidea_superslides.git.

It produces cid2pr.0-1.clone.gz where only the regular vertexes
are compactly mapped v1nam;cluster name
in a one-type vertex graph.
```
github.com_aalmegaard_superslides.git;urc.git
github.com_1bigidea_superslides.git;urc.git
```
In this case both of the projects belong to
cluster urc.git.



One-type vertex graph
----------------------

We may have multiple one-type vertex graphs produced, for example

```
gunzip -c /da3_data/delta/f2p.[0-1].gz | \
  perl /da3_data/delta/connectPbyCID.perl | \
  gzip > /da3_data/delta/f2p.0-1.clone.gz
```
produces links among projects based on matching file names. 


To combine these graphs into a single graph
we use a single-type vertex merging via connectMerge.perl:
```
  gunzip -c /da3_data/delta/{cid2pr,f2p}.0-1.clone.gz  | \
   perl /da3_data/delta/connectMerge.perl | \
   gzip > /da3_data/delta/merge.0-1.clone.gz
```

Lets analyze these graphs:
```
gunzip -c /da3_data/delta/f2p.0-1.clone.gz| cut -d\; -f2 | uniq | wc
  32322   32322 1162562
gunzip -c /da3_data/delta/cid2pr.0-1.clone.gz | cut -d\; -f2 | uniq | wc
  41718   41718 1516816
gunzip -c /da3_data/delta/merge.0-1.clone.gz | cut -d\; -f2 | uniq | wc
  30616   30616 1114775
gunzip -c /da3_data/delta/f2p.[0-1].gz | cut -d\; -f2 | sort -u | wc
 185593  185593 6783166

```

Filename induced graph produces 32322 clusters, while
cid based graph induces 41718 clusters. By merging them
we get 30616 clusters. This is a six-time reduction from
185593 projects in f2p.[0-1].gz.

If we ignore the potential links introduced by cid or file
among different XX, the result:
```
gunzip -c /da3_data/delta/{cid2pr,f2p}.[01].clone.gz | \
perl /da3_data/delta/connectMerge.perl | \
gzip > /da3_data/delta/merge.0-1.pieces.clone.gz
gunzip -c /da3_data/delta/merge.0-1.pieces.clone.gz | cut -d\; -f2 | uniq | wc
33248
```
The number of extra clusters is almost 3K or
10% larger.

connectPbyCID.perl 
==================
Compress input/output:
```
open B, "gunzip < $tmp.clones|";
open A, "|gzip>$tmp.names";
```


Here is what the wrapper does. It first encodes strings as integers
for the project name:
```
my ($id, $v) = split(/\;/, $_, -1);
if (!defined $f2num{$v}){        
   $f2num{$v} = $i+0; 
   print A "$v\n";
   $i++;
}
```

Now encode the content ID as the id for the first
regular vertex it is attached:
```
if (!defined $id2num{$id}){
  $id2num{$id} = $f2num{$v};
}
```

Finally print the link:
```
print B "$id2num{$id} $f2num{$v}\n";
```

The file $tmp.versions contains the adjacencies that
will be read and the connected components produced by
[connect.cpp](https://bitbucket.org/eveng/tasks/src/master/connect.cpp)
(obtained via g++ -O3 -o connect connect.cpp)
```
system ("gunzip < $tmp.versions | $ENV{HOME}/bin/connect |gzip > $tmp.clones");
```

What remains to be done is to create connected subgraphs as hashes:
```
open B, "gunzip < $tmp.clones|";
while (<B>){
   chop();
   my ($f, $cl) = split(/\;/, $_, -1);
   $f=$f+0; $cl=$cl+0;
   $cluster{$cl}{$f}++;
}
```

And decode them:
```
while (my ($k, $v) = each %cluster){
   my %fs = ();
   for my $f (keys %{$v}){
      $fs{$num2f[$f]}++;
   }
   output (\%fs);
}
```

Use the shortest element as the label for the connected subset:
```
sub output {
   my $cl = $_[0];
   my @fs = sort { length($a) <=> length($b) } (keys %{$cl});
   for my $i (0 .. $#fs){
      print "$fs[$i]\;$fs[0]\n";
   }
}
```

connectMerge.perl 
==================

The only difference is that the IDs on both sides
are from the final vertex set:
```
my ($id1, $id2) = split(/\;/, $_, -1);
for my $v ($id1, $id2){
   if (!defined $f2num{$id1}){
      $f2num{$v} = $i+0;
      print A "$v\n";
      $i++;
   }
}
```

Resilience
=========
Doing encoding, connection and decoding in one step
is inviting a disaster: what if some of the components 
run out of RAM?

connectExport.perl 
---------

This simply exports the graph (as .name and .versions files) 
in the integer format that is more space efficient and 
can be used by boost library directly. For example, to get all 
content id to project mappings we can do:

```
seq 0 45 | while read i; do gunzip -c cid2pr.$i.gz; done | \
  perl connectExport.perl c2pNew
```

After running the boost algorithm:
```
gunzip < c2pNew.versions | $ENV{HOME}/bin/connect |gzip > c2pNew.clones
```

The results can be converted back to text via 
```
perl connectImport.perl c2pNew | gzip > c2pNew.map
```

Performance
===========
c2pNew.versions and f2pNew.versions edge files contain a lot of redundant
edges. The boost algorithm has to store each edge separately taking up 
precious RAM space.

connectPrune.per simply reads edges and removes duplicates:
```
gunzip -c c2pNew.versions | perl connectPrune.perl | gzip > c2pNew.versions1
```

The pruned c2pNew.versions1 is processed many times faster:
```
gunzip < c2pNew.versions1 | $ENV{HOME}/bin/connect |gzip > c2pNew.clones
```

Distributed memory processing
=============================

This is described in [openmpi.md](https://bitbucket.org/eveng/tasks/src/master/openmpi.md)



